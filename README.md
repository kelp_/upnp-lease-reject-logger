
# UPnP rejected lease logger

## About

This is a set of patches for two openwrt applications. The first patch modifies miniupnpd to output all of it's rejected leases into a database file.
The second patch integrates the data provided in the database file into the UPnP service panel in LuCI.

## Building

Either copy the patches directly or copy the whole package into your build, compile and enjoy. 

## Usage

All this patch set does is show additional information when viewing the upnp panel in LuCI. No usage instructions needed.



